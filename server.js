require('dotenv').config()
const express = require('express')
const bodyParser = require('body-parser')
require('./config/config.mongoose')
// create express app

const app = express()


app.use(bodyParser.urlencoded({ extended: true })) // parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.json()) // parse requests of content-type - application/json

app.get('/', (req, res) => {
    res.json({ "message": "everything is working fine" })
})

// listen for requests
app.listen(8000, () => {
    console.log("Server is listening on port 8000")
})